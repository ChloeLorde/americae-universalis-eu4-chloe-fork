# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
trade_company_west_africa = {
	color = { 50 0 200 }
	
	provinces = {
		
	}
	
	# Specific
	# Generic
	names = {
		name = "TRADE_COMPANY_WEST_AFRICA_Root_Culture_GetName"
	}
	names = {
		name = "TRADE_COMPANY_WEST_AFRICA_Africa_Trade_Company"
	}
}

#Place
#Holder
trade_company_place_holder = {
	color = { 40 45 60 }
	
	provinces = {
		123 124 
	}
	names = {
		name = "TRADE_COMPANY_PLACE_HOLDER_Root_GetName"
	}
	names = {
		name = "TRADE_COMPANY_PLACE_HOLDER_Place_Holder_Trade_Company"
	}
}
